var https = require('https');
var url = require('url');
var querystring = require('querystring');
var express = require('express');
var app = express();

var host = 'https://view.officeapps.live.com/op/view.aspx?src='; // office web viewer

app.use(express.static('public'));

app.get('/viewer', function(req, res) {
    var targetUrl = req.query.url;
    var contentHTML = '<style>*{margin:0;padding:0;}iframe{border:0}</style>' +
        '<div style="width:100%;height:100%;">' +
        '<iframe id="office_preview" border=0 style="width:100%;height:100%;margin-top:-50px;" src="' + (host + targetUrl) + '"></iframe>' +
        '</div>' +
        '<script>var _p = document.getElementById("office_preview");_p.style.height=_p.offsetHeight+50;</script>';

    res.send(contentHTML);
    // getPage(host + targetUrl, function(data) {
    //     if (data) {

    //         // res.send(data);
    //     } else {
    //         console.log('获取失败');
    //     }
    // })
})

var getPage = function(regUrl, callback) {
    https.get(regUrl, function(res) {
        var data = '';
        res.on('data', function(chunk) {
            data += chunk;
        })
        res.on('end', function() {
            callback(data);
        })
    }).on('error', function() {
        callback(null);
    })
}

// var postPage = function(regUrl, callback) {
//     var post_option = url.parse(regUrl);
//     post_option.method = 'POST';
//     var post_data = querystring.stringify({
//         'access_token': 1,
//         '__VIEWSTATE': ''
//     });
//     post_option.headers = {
//         'Content-Type': 'application/x-www-form-urlencoded',
//         'Content-Length': post_data.length
//     };
//     console.log(post_option);
//     var req = https.request(post_option, function(res) {
//         var data = '';
//         res.on('data', function(chunk) {
//             data += chunk;
//         })
//         res.on('end', function() {
//             callback(data);
//         })
//     }).on('error', function() {
//         callback(null);
//     });

//     req.write(post_data);
//     req.end();
// }


var server = app.listen(8181, function() {
    var host = server.address().address;
    var port = server.address().port;
})